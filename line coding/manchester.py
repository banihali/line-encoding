#positive logic
import numpy as rp
import matplotlib.pyplot as plt
ls1=[]
ls2 = list();
bits=input("input the stream of bits with spaces\n")
input_bits = bits.split(" ")
new_bits = list();
if input_bits[0] == "0":
    new_bits.append(1)
    new_bits.append(-1)
else:
    new_bits.append(-1)
    new_bits.append(1)

for i in range(1,len(input_bits)):
    if input_bits[i] == "1":
        new_bits.append(-new_bits[len(new_bits)-1])
        new_bits.append(new_bits[len(new_bits)-1])
    else:
        new_bits.append(-new_bits[len(new_bits)-1])
        new_bits.append(new_bits[len(new_bits)-1])



for j in range(0,len(new_bits)):
        ls1.append(j)
        ls2.append(0)
        
rep_bits = rp.repeat(new_bits,2)
ls1 = [ls1 * 0.5 for ls1 in range(0,(len(bits)+1))]
rep_ls1 = rp.repeat(ls1,2)
rep_ls1=rep_ls1[1:]
x=rep_ls1.tolist()
x.append(len(input_bits))
#print(rep_bits)
#print(x)

ls2 = rp.repeat(ls2,2)
plt.plot(x,ls2 , color="black")#x-axis
plt.plot(x,rep_bits , linewidth=1,color="blue")#graph plotting
plt.ylabel('y-axis')
plt.xlabel('x-axis')
plt.show()