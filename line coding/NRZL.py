#positive logic
import numpy as rp
import matplotlib.pyplot as plt
ls1=[]
ls2=[]
bits = input("input the stream of bits with spaces\n")
input_bits = bits.split(" ")
new_bits=list()
parts=""
for i in range(0,len(input_bits)):
    if input_bits[i] == "1":
        new_bits.append(1) #duplicate
    else:
        new_bits.append(-1)#duplicate
for j in range(len(input_bits)+1):
    for k in range(2):
        ls1.append(j)
        ls2.append(0)
ls1.pop(0)
ls2.pop(0)
num=len(ls1)
num1=num-1
ls1.pop(num1)
ls2.pop(num1)
rep_bits = rp.repeat(new_bits,2)
print(rep_bits)
print(ls1)
print(ls2)
plt.plot(ls1,ls2 , color="black")#x-axis
plt.plot(ls1,rep_bits , linewidth=1,color="blue")#graph plotting
plt.ylabel('y-axis')
plt.xlabel('x-axis')
plt.show()